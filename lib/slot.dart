import 'session.dart';

/// Information about a [Slot].
class Slot {
  final DateTime startTimestamp;
  final List<Session> sessions = <Session>[];
  
  /// A [Slot] ...
  Slot({
    this.startTimestamp,
  });


  /// Creates a [Slot] from a JSON object.
  Slot.fromSession(Session session)
      : startTimestamp = session.startTimestamp.toUtc() {
    sessions.add(session);
  }
}
