import 'speaker.dart';
import 'room.dart';

/// Information about a [Session].
class Session {
  final String id;
  final String title;
  final String description;
  final String type;
  final String category;
  final String language;
  final DateTime startTimestamp;
  final DateTime endTimestamp;
  final List speakersIds;
  final String roomId;
  final speakers = <Speaker>[];
  Room room;

  /// A [Session] ...
  Session({
    this.id,
    this.title,
    this.description,
    this.type,
    this.category,
    this.language,
    this.startTimestamp,
    this.endTimestamp,
    this.speakersIds,
    this.roomId,
  });

  /// Creates a [Session] from a JSON object.
  Session.fromJson(Map jsonMap)
      : title = jsonMap['title'],
        id = jsonMap['id'],
        description = jsonMap['description'],
        type = jsonMap['type'],
        category = jsonMap['category'],
        language = jsonMap['language'],
        startTimestamp = DateTime.parse(jsonMap['start_timestamp']),
        endTimestamp = DateTime.parse(jsonMap['end_timestamp']),
        speakersIds = jsonMap['speakers_ids'],
        roomId = jsonMap['room_id'];

  void importSpeakers(List<Speaker> speakersList) {
    if (speakersIds.length == 0) return;

    speakers.clear();

    for (var speakerId in speakersIds) {
      for (var speaker in speakersList) {
        if (speaker.id == speakerId) {
          speakers.add(speaker);
          break;
        }
      }
    }
  }

  void importRoom(List<Room> roomsList) {
    if (roomId == null) {
      this.room = null;
    } else {
      for (var room in roomsList) {
        if (room.id == roomId) {
          this.room = room;
          return;
        }
      }
    }
  }
}
