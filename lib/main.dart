import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'dart:convert';
import 'dart:async';

import 'session.dart';
import 'room.dart';
import 'speaker.dart';
import 'slot.dart';
import 'session_route.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Demo',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
        cardColor: Colors.grey[300],
        accentColor: Colors.blue,
        primaryColorLight: Colors.lightBlueAccent,
        highlightColor: Colors.white,
      ),
      home: new MyHomePage(title: 'Devfest Flutter'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
//  final _sessionsJ1 = <Session>[];
//  final _sessionsJ2 = <Session>[];

  final _slotsJ1 = <Slot>[];
  final _slotsJ2 = <Slot>[];

  // final _rooms = <Room>[];
  // final _speakers = <Speaker>[];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: Text(widget.title),
          centerTitle: true,
          bottom: TabBar(
            tabs: [
              Tab(text: '19 Octobre'),
              Tab(text: '20 Octobre'),
            ],
            labelStyle: TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold),
          ),
        ),
        body: TabBarView(
          children: [
            _getSlotWidget(_slotsJ1),
            _getSlotWidget(_slotsJ2),
          ],
        ),
      ),
    );
  }

  Widget _getSlotWidget(List<Slot> slots) {
    return ListView(
      children: slots.map((Slot slot) {
        return new Column(
          children: <Widget>[
            new Container(
              color: Colors.blue[100],
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      new DateFormat.Hm().format(slot.startTimestamp.toLocal()),
                      style: TextStyle(
                        fontSize: 20.0,
                        //     color: Theme.of(context).highlightColor,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            //),
            Column(
              children: getSessionListWidget(slot),
            ),
          ],
        );
        //       );
      }).toList(),
    );
  }

  List<Widget> getSessionListWidget(Slot slot) {
    List<Widget> builder = <Widget>[];

    for (var session in slot.sessions) {
      builder.add(InkWell(
        borderRadius: BorderRadius.circular(8.0),
        onTap: () => _navigateToSession(context, session),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(16.0),
              child: Text(
                session.title,
                textAlign: TextAlign.start,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
              ),
            ),
            new Padding(
              padding: const EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: sessionWidgets(session),
              ),
            ),
          ],
        ),
      ));
      builder.add(Divider(
        color: Colors.black,
        height: 5.0,
      ));
    }
    //Remove the last divider of the list
    builder.removeLast();
    return builder;
  }

  List<Widget> sessionWidgets(Session session) {
    List<Widget> builder = <Widget>[];

    if (session.room != null) {
      builder.add(Expanded(
        child: Text(
          session.room.name,
          textAlign: TextAlign.start,
        ),
      ));
    }
    if (session.speakers.length > 0) {
      builder.add(Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: session.speakers.map((Speaker speaker) {
          return Text(
            speaker.name,
            textAlign: TextAlign.end,
            style: TextStyle(fontWeight: FontWeight.w500),
          );
        }).toList(),
      ));
    }

    return builder;
  }

  @override
  Future<void> didChangeDependencies() async {
    super.didChangeDependencies();

    if (_slotsJ1.isEmpty) {
      await _retrieveLocalPlanning();
    }
  }

  /// Retrieves a list of [session]
  Future<void> _retrieveLocalPlanning() async {
    final json =
        DefaultAssetBundle.of(context).loadString('assets/data/seed.json');
    final data = JsonDecoder().convert(await json);
    if (data is! Map) {
      throw ('Data retrieved from API is not a Map');
    }

    final List<Session> sessions = data['sessions']
        .map<Session>((dynamic data) => Session.fromJson(data))
        .toList();

    final List<Room> rooms =
        data['rooms'].map<Room>((dynamic data) => Room.fromJson(data)).toList();

    final List<Speaker> speakers = data['speakers']
        .map<Speaker>((dynamic data) => Speaker.fromJson(data))
        .toList();

    final sessionsJ1 = <Session>[];
    final sessionsJ2 = <Session>[];

    for (var session in sessions) {
      session.importSpeakers(speakers);
      session.importRoom(rooms);
      DateTime jour2 = DateTime.utc(2017, 10, 20);
      if (session.startTimestamp.isAfter(jour2)) {
        sessionsJ2.add(session);
      } else {
        sessionsJ1.add(session);
      }
    }

    sessionsJ1.sort((a, b) => a.startTimestamp.compareTo(b.startTimestamp));

    final jour1 = <Slot>[];

    for (var session in sessionsJ1) {
      if ((jour1.length == 0) ||
          (session.startTimestamp.isAfter(jour1.last.startTimestamp))) {
        jour1.add(Slot.fromSession(session));
      } else {
        jour1.last.sessions.add(session);
      }
    }

    final jour2 = <Slot>[];

    sessionsJ2.sort((a, b) => a.startTimestamp.compareTo(b.startTimestamp));

    for (var session in sessionsJ2) {
      if ((jour2.length == 0) ||
          (session.startTimestamp.isAfter(jour2.last.startTimestamp))) {
        jour2.add(Slot.fromSession(session));
      } else {
        jour2.last.sessions.add(session);
      }
    }

    setState(() {
      _slotsJ1.clear();
      _slotsJ1.addAll(jour1);
      _slotsJ2.clear();
      _slotsJ2.addAll(jour2);
    });
  }

  /// Navigates to the [ConverterRoute].
  void _navigateToSession(BuildContext context, Session session) {
    print("Navigate...");
    Navigator.of(context).push(MaterialPageRoute<Null>(
      builder: (BuildContext context) {
        return Scaffold(
          appBar: AppBar(
            elevation: 1.0,
            title: Text("Détails de la session"),
            centerTitle: true,
            //          backgroundColor: color,
          ),
          body: SessionRoute(
            session: session,
          ),
          // This prevents the attempt to resize the screen when the keyboard
          // is opened
          resizeToAvoidBottomPadding: false,
        );
      },
    ));
  }
}
