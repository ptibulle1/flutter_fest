/// Information about a [Speaker].
class Speaker {
  final String id;
  final String name;
  final String company;
  final String country;
  final String bio;
//  final List tags;
  final String photoUrl;
 // final List<String> social_links;

  /// A [Speaker] ...
  Speaker({
    this.id,
    this.name,
    this.company,
    this.country,
    this.bio,
  //  this.tags,
    this.photoUrl
  });

  /// Creates a [Speaker] from a JSON object.
  Speaker.fromJson(Map jsonMap)
      : name = jsonMap['name'],
        id = jsonMap['id'],
        company = jsonMap['company'],
        country = jsonMap['country'],
        bio = jsonMap['bio'],
  //      tags = jsonMap['tags'],
        photoUrl = jsonMap['photo_url'];

}
