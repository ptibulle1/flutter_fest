/// Information about a [Room].
class Room {
  final String id;
  final String name;

  /// A [Room] ...
  Room({
    this.id,
    this.name,
  });

  /// Creates a [Room] from a JSON object.
  Room.fromJson(Map jsonMap)
      : name = jsonMap['name'],
        id = jsonMap['id'];
}
