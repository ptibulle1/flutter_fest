import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:intl/intl.dart';

import 'session.dart';
import 'room.dart';
import 'speaker.dart';

/// Converter screen where users can input amounts to convert.
///
/// Currently, it just displays a list of mock units.
///
/// While it is named SessionRoute, a more apt name would be ConverterScreen,
/// because it is responsible for the UI at the route's destination.
class SessionRoute extends StatelessWidget {
  final Session session;

  /// This [SessionRoute] requires the session to not be null.
  const SessionRoute({
    @required this.session,
  }) : assert(session != null);

  @override
  Widget build(BuildContext context) {
    return new Padding(
      padding: const EdgeInsets.all(8.0),
      child: ListView(
        children: buildListWidget(context),
      ),
    );
  }

  static const List<CategoryColor> categories = [
    CategoryColor("discovery", Colors.red),
    CategoryColor("web", Colors.blue),
    CategoryColor("mobile", Colors.green),
    CategoryColor("cloud", Colors.orange),
  ];

  List<Widget> buildListWidget(BuildContext context) {
    var builder = <Widget>[];

    builder.add(Padding(
      padding: EdgeInsets.only(bottom: 16.0),
      child: Text(
        session.title,
        textAlign: TextAlign.start,
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 26.0),
      ),
    ));

    builder.add(Text(
      DateFormat("EEEE d MMMM y").format(session.startTimestamp.toLocal()) +
          ", " +
          DateFormat.Hm().format(session.startTimestamp.toLocal()) +
          " - " +
          DateFormat.Hm().format(session.endTimestamp.toLocal()),
      textAlign: TextAlign.start,
      style: TextStyle(fontWeight: FontWeight.w600, fontSize: 14.0),
    ));

    if (session.room != null) {
      builder.add(Text(
        "Salle " + session.room.name,
        textAlign: TextAlign.start,
        style: TextStyle(fontWeight: FontWeight.w600, fontSize: 14.0),
      ));
    }
    if (session.language != null) {
      builder.add(Padding(
        padding: EdgeInsets.only(bottom: 16.0),
        child: Text(
          "Langue : " + session.language,
          textAlign: TextAlign.start,
          style: TextStyle(fontWeight: FontWeight.w600, fontSize: 14.0),
        ),
      ));
    }

    if (session.description != null) {
      builder.add(Padding(
        padding: EdgeInsets.only(bottom: 16.0),
        child: Text(
          session.description,
          textAlign: TextAlign.start,
          style: TextStyle(fontWeight: FontWeight.w400, fontSize: 16.0),
        ),
      ));
    }

    if (session.category != null) {
      CategoryColor cc = categories.firstWhere((CategoryColor c) => c.category == session.category, orElse: () => CategoryColor("default", Colors.grey[700]));
      builder.add(Align(
        alignment: Alignment.bottomLeft,
        child: Container(
          //elevation: 0.0,
          color: cc.color,
          child: new Padding(
            padding: const EdgeInsets.fromLTRB(8.0, 4.0, 8.0, 4.0),
            child: Text(
              session.category,
              style: TextStyle(
                fontSize: 20.0,
                color: Theme.of(context).highlightColor,
                decorationColor: cc.color
              ),
            ),
          ),
        ),
      ));
    }

    if (session.speakers.length > 0) {
      builder.add(Padding(
        padding: const EdgeInsets.all(8.0),
        child: Divider(
          height: 20.0,
          color: Colors.black,
        ),
      ));
      builder.add(Column(
        children: session.speakers.map((Speaker speaker) {
          return new Padding(
            padding: const EdgeInsets.only(bottom: 16.0),
            child: new Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  width: 50.0,
                  height: 50.0,
                  decoration: new BoxDecoration(
                    shape: BoxShape.circle,
                    image: new DecorationImage(
                      fit: BoxFit.fill,
                      image: NetworkImage(speaker.photoUrl),
                    ),
                  ),
                ),
                new Flexible(
                  child: new Padding(
                    padding: const EdgeInsets.only(left: 16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: buildSpeaker(context, speaker),
                    ),
                  ),
                ),
              ],
            ),
          );
        }).toList(),
      ));
    }

    return builder;
  }

  List<Widget> buildSpeaker(BuildContext context, Speaker speaker) {
    var builder = <Widget>[];
    builder.add(Text(
      speaker.name,
      textAlign: TextAlign.start,
      style: TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: 16.0,
        color: Theme.of(context).accentColor,
      ),
    ));
    if (speaker.company != null) {
      builder.add(Text(
        speaker.company,
        textAlign: TextAlign.start,
        style: TextStyle(fontWeight: FontWeight.w500, fontSize: 16.0),
      ));
    }

    builder.add(Text(
      speaker.bio,
      textAlign: TextAlign.start,
      style: TextStyle(fontWeight: FontWeight.w400, fontSize: 16.0),
    ));

    return builder;
  }
}

class CategoryColor {
  final String category;
  final MaterialColor color;

  const CategoryColor(this.category, this.color);
}
